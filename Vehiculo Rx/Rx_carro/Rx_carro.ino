  /**
 * Codigo receptor Crop Rover
 *
 * David Andres Caliz Hernandez
 */
#include "Arduino.h"
#include "RF24.h"
#include "SPI.h" //MISO = 50, MOSI = 51, SCK = 52, SS = 53<<no usado
//pines *faltan los sensores
#define pin_ce 48
#define pin_cs 49
#define Motor1 10
#define Motor2 11
#define rvrsM1 12
#define rvrsM2 13
#define buzzer 4
#define ledSgnl 45
//cosas del radio
byte canal[6] = "CONTR"; //5 Bytes
RF24 radio(pin_ce, pin_cs);

/**
 * info datos[] *Esto sirve para anotar que hace cada boton del control
 * 0 BtnA	subir motor sensores
 * 1 BtnB	toggle
 * 2 BtnC	bajar motor sensores
 * 3 BtnD	llamar metodo para medir datos
 * 4 BtnE
 * 5 BtnF
 * 6 BtnK	Buzzer
 * 7 JX		Joystick movimiento
 * 8 JY		Joystick movimiento
 */
//Variables globales
int datos[9]; //un arreglo donde se coloca cada entrada del control
unsigned long ultimoDato;
int JX;
int JY;
int buzz;
void setup()
{
	pinMode(Motor1, OUTPUT);
	pinMode(Motor2, OUTPUT);
	pinMode(rvrsM1, OUTPUT);
	pinMode(rvrsM2, OUTPUT);
	pinMode(buzzer, OUTPUT);
	pinMode(ledSgnl, OUTPUT);
	pinMode(44, OUTPUT);
	digitalWrite(44, LOW);

	//Serial.begin(9600);
	radio.begin();
	radio.setDataRate(RF24_250KBPS);
	radio.setPALevel(RF24_PA_MAX);
	radio.openReadingPipe(1, canal);
	radio.startListening();
}


void loop()
{

	if(radio.available())
	{
		radio.read(&datos, sizeof(datos));
		ultimoDato = millis();
		digitalWrite(ledSgnl, HIGH);
		/**
		Serial.println("datos Recibidos");
		Serial.println((float)sizeof(datos));
		Serial.println(datos[0]);
		Serial.println(datos[1]);
		Serial.println(datos[2]);
		Serial.println(datos[3]);
		Serial.println(datos[4]);
		Serial.println(datos[5]);
		Serial.println(datos[6]);
		Serial.println(datos[7]);
		Serial.println();
		*/
	}
	//si pasa mas de un segundo sin recibir nueva info del control
	if((millis() - ultimoDato) > 900 ) noSignal();
	//Extraer la posicion del joystick
	buzz = datos[6];
	JX = datos[7];
	JY = datos[8];
	//otros controles
		//Pito
		if(buzz < 1)
		{
			digitalWrite(buzzer, HIGH);
		}
		else
		{
			digitalWrite(buzzer,LOW);
		}
//Control de movimiento
//Detenido
	if(puntoMuertoX() && puntoMuertoY())
	{
		analogWrite(Motor1, 0);
		analogWrite(Motor2, 0);
		digitalWrite(rvrsM1, LOW);
		digitalWrite(rvrsM2, LOW);
	}
//Giro sobre el mismo eje CW
	if(puntoMuertoY() && JX >= 539)
	{
		int pwm = map(JX, 539, 1023, 0, 255);
		digitalWrite(rvrsM2, HIGH);
		analogWrite(Motor1, pwm);
		analogWrite(Motor2, pwm);
	}
//Giro sobre el mismo eje CCW
	if(puntoMuertoY() && JX <= 537)
	{
		int pwm = map(JX, 537, 0, 0, 255);
		digitalWrite(rvrsM1, HIGH);
		analogWrite(Motor1, pwm);
		analogWrite(Motor2, pwm);
	}
//andar hacia adelante
	if(JY >= 521)
	{
		int vTotal = map(JY, 521, 1023, 0, 255);
		int mod1 = 0;
		int mod2 = 0;
		if(JX < 539)
		{
		mod1 = map(JX, 0, 538, vTotal, 0);
		}
		if(JX > 539)
		{
		mod2 = map(JX, 538, 1023, 0, vTotal);
		}
		digitalWrite(rvrsM1, LOW);
		digitalWrite(rvrsM2, LOW);
		analogWrite(Motor1, vTotal-mod1);
		analogWrite(Motor2, vTotal-mod2);
	}
//andar hacia atras
	if(JY <=519)
	{
		int vTotal = map(JY, 519, 0, 0, 255);
		int mod1 = 0;
		int mod2 = 0;
		if(JX < 539)
		{
		mod1 = map(JX, 0, 538, vTotal, 0);
		}
		if(JX > 539)
		{
		mod2 = map(JX, 538, 1023, 0, vTotal);
		}
		digitalWrite(rvrsM1, HIGH);
		digitalWrite(rvrsM2, HIGH);
		analogWrite(Motor1, vTotal-mod1);
		analogWrite(Motor2, vTotal-mod2);
	}

	//Tomar datos

	if(datos[3] == 0) //mal, implementar como arriba
	{
		bool ok = tomarDatos();
    if(ok) {}//prender un led o algo
  
	}

delay(20);//hummm
}//Aqui termina void loop()

void noSignal() //Parar todos
{
	digitalWrite(ledSgnl, LOW);
	datos[6] = 1;
	datos[7] = 538;
	datos[8] = 520;
	//Serial.println("Sin se�al");
	delay(100);
	//if(radio.available()) loop();
	//Parar el motor que baja los sensores
}
bool puntoMuertoX()
{
	int min = 537; //cambiar valores min max
	int max = 539;
	if(min < JX && JX < max){
		return true;
	}
	else {
		return false;
	}
}
bool puntoMuertoY()
{
	int min = 519;
	int max = 521;
	if(min < JY && JY < max){
		return true;
	}
	else {
		return false;
	}
}
