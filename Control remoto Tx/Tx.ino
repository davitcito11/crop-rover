/**
 * Codigo transmisor control remoto Crop Rover
 *
 * David Andres Caliz Hernandez
 */
#include "Arduino.h"
#include "RF24.h"
#include "SPI.h" // MISO = 14, SCK = 15, MOSI = 16

//Pines

#define pin_ce 0
#define pin_cs 1
#define BtnA 2
#define BtnB 3
#define BtnC 4
#define BtnD 5
#define BtnE 6
#define BtnF 7
#define BtnK 8
#define JX A0
#define JY A1
#define led1 11
#define led2 12
//variables para el radio
byte canal[6] = "CONTR"; //5 Bytes
RF24 radio(pin_ce, pin_cs);
int datos[9]; //tama�o maximo 32 bytes
// Variables para los motores ignorar*
/**
float posX;
float posY;
float M1F;
float M1R;
float M2F;
float M2R;
*/
void setup()
{
	//setear los pines 1-8 como entradas
	for(int npin = BtnA; npin <= BtnK;npin++) pinMode(npin,INPUT_PULLUP);

//Serial.begin(9600);
radio.begin();
radio.setDataRate(RF24_250KBPS);
radio.setPALevel(RF24_PA_MAX); //Cambiar a MAX tras terminar pruebas
radio.stopListening();
radio.openWritingPipe(canal);
}

void loop()
{
datos[0] = digitalRead(BtnA);
datos[1] = digitalRead(BtnB);
datos[2] = digitalRead(BtnC);
datos[3] = digitalRead(BtnD);
datos[4] = digitalRead(BtnE);
datos[5] = digitalRead(BtnF);
datos[6] = digitalRead(BtnK);
datos[7] = analogRead(JX);
datos[8] = analogRead(JY);
/**
Serial.println(datos[0]);
Serial.println(datos[1]);
Serial.println(datos[2]);
Serial.println(datos[3]);
Serial.println(datos[4]);
Serial.println(datos[5]);
Serial.println(datos[6]);
Serial.println(datos[7]);
*/
	if(radio.write(&datos, sizeof(datos)))
	{
		digitalWrite(led2, HIGH); digitalWrite(led1, LOW);
		//Serial.println("datos enviados");
	}
	else
	{
		digitalWrite(led1,HIGH); digitalWrite(led2, LOW);
		//Serial.println("Error");
	}

	delay(20);
}

/** migrar al programa del carro
//realizar mapeos con switch/case?
if(500 < posX < 515){
float M1 = map(posX, 0,512, 0, 255);
datos[7]= M1;
*/
